﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MyAPIs.Model;
using MyAPIs.Models;

namespace MyAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyCoffeeStore : ControllerBase
    {
        public readonly APIDBContext _context;
        private readonly IConfiguration _config;


        public MyCoffeeStore(APIDBContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        #region ------- GetProducts -----------
        [HttpGet("GetProducts")]
        public ActionResult<List<Product>> GetProducts()
        {
            List<ProductDetail> Prodetail = new List<ProductDetail>();

            Prodetail = _context.ProductDetails.ToList();

            return _context.Products.ToList();
        }










        #endregion

        #region ------- GetTransection -----------
        [HttpGet("GetTransection/{id}")]
        public ActionResult<List<Transaction>> GetTransectionAsync(int id)
        {
            try
            {
                List<TransactionDetail> TranDetail = new List<TransactionDetail>();
                TranDetail = _context.TransactionDetails.ToList();

                var data = _context.Transactions.Where(x => x.TransactionId == id).ToList();

                if(data.Count == 0)
                {
                    throw new Exception($"ไม่พบ TransectionID { id } นี้"); 
                }
                return data;
            }catch(Exception ex)
            {
                return StatusCode(500, new ResponseMessage { statusCode = 500, message = ex.Message });
            }













        }
        #endregion

        #region  ------- CreateNewProduct ---
        [HttpPost("CreateNewProduct")]
        public async Task<ActionResult<ProductDetail>> CreateNewProduct([FromBody] ProductDetail prod)
        {
            await _context.ProductDetails.AddAsync(prod);
            await _context.SaveChangesAsync();

            return prod;

        }
        #endregion

        #region ------ UpdateProduct -----
        [HttpPost("UpdateProduct/{id}")]
        public async Task<ActionResult<Product>> UpdateProduct([FromBody] Product prod)
        {
            var isBadRequest = false;
            try
            {
                if (prod == null)
                    throw new Exception("กรุณากรอกข้อมูลสินค้า");

                if (prod.ProductId < 1)
                    throw new Exception("กรุณากรอกรหัสสินค้าหลักที่ต้องการแก้ไข");

                if (string.IsNullOrEmpty(prod.ProductName))
                    throw new Exception("กรุณากรอกชื่อสินค้า");


                isBadRequest = false;
                Product result = null;
                //แก้ไขสินค้าหลักและสินค้าย่อย
                if (prod.ProductDetails != null)
                {
                    var _prod = await _context.Products.Include(x => x.ProductDetails).FirstOrDefaultAsync(x => x.ProductId == prod.ProductId);
                    if (_prod == null)
                    {
                        isBadRequest = true;
                        throw new Exception("แก้ไขล้มเหลว: ไม่พบรหัสสินค้าหลัก");
                    }

                    _prod.ProductName = prod.ProductName;

                    foreach (var pd in prod.ProductDetails)
                    {
                        var _productDetail = _prod.ProductDetails.Where(x => x.ProductDetailId == pd.ProductDetailId).FirstOrDefault();
                        if (_productDetail == null)
                        {
                            isBadRequest = true;
                            throw new Exception($"แก้ไขล้มเหลว: ไม่พบรหัสสินค้ารอง ({pd.ProductDetailId})");
                        }
                        else
                        {
                            _productDetail.ProductDetailName = pd.ProductDetailName;
                            _productDetail.ProductDetailPrice = pd.ProductDetailPrice;
                        }
                    }

                    _context.Products.Update(_prod);
                    await _context.SaveChangesAsync();
                    result = _prod;
                }
                else
                {
                    var _prod = await _context.Products.FirstOrDefaultAsync(x => x.ProductId == prod.ProductId);
                    _prod.ProductName = prod.ProductName;
                    _context.Products.Update(_prod);
                    result = _prod;
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                if (isBadRequest)
                {
                    return BadRequest(new ResponseMessage { statusCode = 400, message = ex.Message });
                }
                else
                {
                    return StatusCode(500, new ResponseMessage { statusCode = 500, message = ex.Message });
                }
            }
        }

        #endregion

        #region -------- DeleteProduct ------------
        [HttpDelete("DeleteProduct/{id}")]
        public async Task<ActionResult<ResponseMessage>> DeleteProduct(int id)
        {
            try
            {
                var prod = await _context.Products.Where(x => x.ProductId == id).FirstOrDefaultAsync();
                if (prod == null)
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = $"ไม่พบรหัสสินค้า{id}" });

                _context.Remove(prod);
                await _context.SaveChangesAsync();

                //---------- test show data is delete --------
                var data = _context.Products.Where(c => c.ProductId == id).ToList();





                return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = $"ลบข้อมูลสินค้า {id} เรียบร้อย" });
            }
            catch (Exception ex)
            {
                return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
            }
        }
        #endregion

        #region ----- GenerateJWToken + employee -------
        private string GenerateJWToken(string userGuid)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddHours(1);

            var token = new JwtSecurityToken(
                issuer: _config["JwtIssuer"],
                audience: _config["JwtAudience"],
                claims: new List<Claim> { new Claim("user_guid", userGuid) },
                expires: expires,
                signingCredentials: creds
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }



        public class ResponseMessage
        {
            public int statusCode { get; set; }
            public string message { get; set; }
        }

        [AllowAnonymous]
        [HttpPost("RegisterNewEmployee")]
        public async Task<ActionResult<ResponseMessage>> RegisterEmployee([FromBody] Employee user)
        {
            try
            {
                var chkUser = await _context.Users.FirstOrDefaultAsync(x => x.userName.Equals(user.userName));
                if (chkUser != null)
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "User name ซ้ำจ้า!!" });

                User _user = new User
                {
                    guid = Guid.NewGuid(),
                    userName = user.userName.Trim(),
                    password = user.password.Trim(),
                    firstName = user.firstName.Trim(),
                    lastName = user.lastName.Trim(),
                    email = user.email.Trim(),
                    mobile = user.mobile.Trim()
                };

                await _context.Users.AddAsync(_user);
                await _context.SaveChangesAsync();

                string token = GenerateJWToken(_user.guid.ToString());
                return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = token });
            }
            catch (Exception ex)
            {
                return StatusCode((int)System.Net.HttpStatusCode.InternalServerError,
                    new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpGet("GetNewTokenForExistingEmployee")]
        public async Task<ActionResult<string>> GetNewTokenForExistingEmployee([FromQuery] string userName, [FromQuery] string password)
        {
            try
            {
                if (string.IsNullOrEmpty(userName))
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "กรอก User name ด้วยจ้า!!" });
                if (string.IsNullOrEmpty(password))
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "กรอก Password ด้วยจ้า!!" });

                var _user = await _context.Users.FirstOrDefaultAsync(x => x.userName.Equals(userName) && x.password.Equals(password));
                if (_user == null)
                    return BadRequest(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.BadRequest, message = "User name  หรือ password ไม่ถูกต้อง" });

                string token = GenerateJWToken(_user.guid.ToString());
                return Ok(new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.OK, message = token });
            }
            catch (Exception ex)
            {
                return StatusCode((int)System.Net.HttpStatusCode.InternalServerError,
                    new ResponseMessage { statusCode = (int)System.Net.HttpStatusCode.InternalServerError, message = ex.Message });
            }
        }

        #endregion


    }
}
