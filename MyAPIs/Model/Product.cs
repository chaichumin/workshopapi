﻿using MyAPIs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace MyAPIs.Model
{
    
    public class Product 
    {
        [Key]
        public int ProductId { get; set; }

        [Required]
        public string ProductName { get; set; }

        public bool IsDeleted { get; set; }

        public List<ProductDetail> ProductDetails { get; set; }


    }
}
