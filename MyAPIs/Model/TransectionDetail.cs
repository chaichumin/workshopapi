﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace MyAPIs.Models
{
    public class TransactionDetail
    {
        [Key]
        public int TransactionDetailId { get; set; }

        public int TransactionId { get; set; }

        public int ProductDetailId { get; set; }

        public int Quantity { get; set; }

        public decimal TotalPrice { get; set; }

        //[ForeignKey(nameof(TransactionId))]
        //public Transaction Transaction { get; set; }

        //[ForeignKey(nameof(ProductDetailId))]
        //public ProductDetail ProductDetail { get; set; }




    }
}
