﻿using MyAPIs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace MyAPIs.Model
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }

        public int UserId { get; set; }

        public decimal ReceivedMoney { get; set; }

        public decimal ReturnMoney { get; set; }

        public decimal TotalTransactionDetailPrice { get; set; }

        public DateTime TransactionDate { get; set; }


        public List<TransactionDetail> TransactionDetails { get; set; }




    }
}
