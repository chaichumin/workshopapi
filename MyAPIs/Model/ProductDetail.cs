﻿using MyAPIs.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyAPIs.Models
{
    public class ProductDetail 
    {

        [Key]
        public int ProductDetailId { get; set; }

        [ForeignKey(nameof(ProductId))]
        public int ProductId { get; set; }

        public string ProductDetailName { get; set; }

        public decimal ProductDetailPrice { get; set; }


        //public Product Product { get; set; }



    }
}
