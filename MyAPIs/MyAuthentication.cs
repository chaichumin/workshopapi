﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using MyAPIs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MyAPIs
{
    public class MyAuthentication : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                var dbContext = context.HttpContext.RequestServices.GetService(typeof(APIDBContext)) as APIDBContext;
                var config = context.HttpContext.RequestServices.GetService(typeof(IConfiguration)) as IConfiguration;
                var user = context.HttpContext.User;
                if (!user.Identity.IsAuthenticated)
                    throw new Exception();

                string user_guid = user.Claims.First(c => c.Type == "user_guid").Value;
                if (dbContext.Users.FirstOrDefault(x => x.guid == new Guid(user_guid)) == null)
                    throw new Exception();

            }
            catch(Exception ex)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
