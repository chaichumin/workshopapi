﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MyAPIs.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using MyAPIs.Models;

namespace MyAPIs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero, //remove delay of token when expire
                        RequireExpirationTime = true,
                        ValidateLifetime = true
                    };
                });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(name: "v1", new OpenApiInfo { Title = "My APIs", Version = "v1" });

                c.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
            });

            services.AddDbContext<APIDBContext>(o => o.UseInMemoryDatabase("MyCoffeeStore"));
            //services.AddMvc();
            services.AddScoped<IDbInitializer, DBInitializer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(url: "/swagger/v1/swagger.json", name: "My APIs V1");
            });


            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseMvc();
            dbInitializer.DataSeeding().Wait();
        }
        public interface IDbInitializer
        {
            Task DataSeeding();
        }
        public class DBInitializer : IDbInitializer
        {
            private readonly APIDBContext _context;
            public DBInitializer(APIDBContext context)
            {
                _context = context;
            }

            public async Task DataSeeding()
            {
                try
                {
                    await _context.AddRangeAsync(new List<Product>
                    {
                        new Product {ProductId = 112, ProductName = "เอสเปรสโซ่ ร้อน", IsDeleted = false},
                        new Product {ProductId = 113, ProductName = "เอสเปรสโซ่ เย็น", IsDeleted = false},
                        new Product {ProductId = 114, ProductName = "เอสเปรสโซ่ ปั่น", IsDeleted = false},
                        new Product {ProductId = 115, ProductName = "อเมซอน ร้อน", IsDeleted = false},
                        new Product {ProductId = 116, ProductName = "อเมซอน เย็น", IsDeleted = false},
                        new Product {ProductId = 117, ProductName = "แบล็คคอฟฟี่ ร้อน", IsDeleted = false},
                        new Product {ProductId = 118, ProductName = "แบล็คคอฟฟี่ เย็น", IsDeleted = false},
                        new Product {ProductId = 119, ProductName = "คาปูชิโน่ ร้อน", IsDeleted = false},
                        new Product {ProductId = 120, ProductName = "คาปูชิโน่ เย็น", IsDeleted = false},
                        new Product {ProductId = 121, ProductName = "คาปูชิโน่ ปั่น", IsDeleted = false},
                        new Product {ProductId = 122, ProductName = "ลาเต้ ร้อน", IsDeleted = false},
                        new Product {ProductId = 123, ProductName = "ลาเต้ เย็น", IsDeleted = false},
                        new Product {ProductId = 124, ProductName = "ลาเต้ ปั่น",  IsDeleted = false},
                        new Product {ProductId = 125, ProductName = "มอคค่า ร้อน", IsDeleted = false},
                        new Product {ProductId = 126, ProductName = "มอคค่า เย็น", IsDeleted = false},
                        new Product {ProductId = 127, ProductName = "มอคค่า ปั่น", IsDeleted = false}
                    });

                    await _context.AddRangeAsync(new List<ProductDetail>
                    {
                        new ProductDetail { ProductId = 112,ProductDetailId = 123, ProductDetailName = "กาแฟสดจากชาวดอย", ProductDetailPrice = 1590},
                        new ProductDetail { ProductId = 113,ProductDetailId = 456, ProductDetailName = "กาแฟสดจาก USA",  ProductDetailPrice = 1320},
                        new ProductDetail { ProductId = 114,ProductDetailId = 789, ProductDetailName = "กาแฟสดจาก LA",   ProductDetailPrice = 1680},
                        new ProductDetail { ProductId = 115,ProductDetailId = 101, ProductDetailName = "กาแฟสดจาก BE ", ProductDetailPrice = 1990},
                        new ProductDetail { ProductId = 116,ProductDetailId = 121, ProductDetailName = "กาแฟสดจาก MX",  ProductDetailPrice = 2650},
                        new ProductDetail { ProductId = 117,ProductDetailId = 141, ProductDetailName = "กาแฟสดจาก NO",  ProductDetailPrice = 1230},
                        
                       
                    });

                    await _context.AddRangeAsync(new List<Transaction>
                    {
                        new Transaction { ReceivedMoney = 100 ,ReturnMoney= 130, TransactionId = 1,TotalTransactionDetailPrice = 3526, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 1000,ReturnMoney= 850, TransactionId = 2,TotalTransactionDetailPrice = 3456, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 500 ,ReturnMoney= 320, TransactionId = 3,TotalTransactionDetailPrice = 2345, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 100 ,ReturnMoney= 130, TransactionId = 4,TotalTransactionDetailPrice = 2563, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 1000,ReturnMoney= 850, TransactionId = 5,TotalTransactionDetailPrice = 4352, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 500 ,ReturnMoney= 320, TransactionId = 6,TotalTransactionDetailPrice = 1234, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 100 ,ReturnMoney= 130, TransactionId = 7,TotalTransactionDetailPrice = 5231, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 1000,ReturnMoney= 850, TransactionId = 8,TotalTransactionDetailPrice = 9200, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 500 ,ReturnMoney= 320, TransactionId = 9,TotalTransactionDetailPrice = 6524, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 100 ,ReturnMoney= 130, TransactionId = 10,TotalTransactionDetailPrice = 4545, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 1000,ReturnMoney= 850, TransactionId = 11,TotalTransactionDetailPrice = 1214, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 500 ,ReturnMoney= 320, TransactionId = 12,TotalTransactionDetailPrice = 5424, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 100 ,ReturnMoney= 130, TransactionId = 13,TotalTransactionDetailPrice = 2682, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 1000,ReturnMoney= 850, TransactionId = 14,TotalTransactionDetailPrice = 9200, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 500 ,ReturnMoney= 320, TransactionId = 15,TotalTransactionDetailPrice = 4315, TransactionDate = DateTime.Now},
                        new Transaction { ReceivedMoney = 500 ,ReturnMoney= 320, TransactionId = 16,TotalTransactionDetailPrice = 2680, TransactionDate = DateTime.Now},



                    });

                    await _context.AddRangeAsync(new List<User>
                    {
                        new User { firstName= "david", lastName = "com" , email = "www@gmail.com",mobile="0812345671"},
                        new User { firstName= "david2", lastName = "com2",email = "www2@gmail.com",mobile="0812345672"},
                        new User { firstName= "david3", lastName = "com3",email = "www3@gmail.com",mobile="0812345673"},
                        new User { firstName= "david4", lastName = "com4",email = "www4@gmail.com",mobile="0812345674"},
                        new User { firstName= "david5", lastName = "com5",email = "www5@gmail.com",mobile="0812345675"},
                        new User { firstName= "david6", lastName = "com6",email = "www6@gmail.com",mobile="0812345676"},
                        new User { firstName= "david7", lastName = "com7",email = "www7@gmail.com",mobile="0812345677"},
                        new User { firstName= "david8", lastName = "com8",email = "www8@gmail.com",mobile="0812345678"},


                    });

                    await _context.AddRangeAsync(new List<TransactionDetail>
                    {
                        new TransactionDetail { TransactionDetailId = 1 , TransactionId = 1,ProductDetailId = 123, Quantity = 10, TotalPrice = 2350},
                        new TransactionDetail { TransactionDetailId = 2 , TransactionId = 2,ProductDetailId = 456, Quantity = 10, TotalPrice = 2350},
                        new TransactionDetail { TransactionDetailId = 3 , TransactionId = 10,ProductDetailId = 789, Quantity = 20, TotalPrice = 2350},
                        new TransactionDetail { TransactionDetailId = 4 , TransactionId = 11,ProductDetailId = 101, Quantity = 30, TotalPrice = 2350},
                        new TransactionDetail { TransactionDetailId = 5 , TransactionId = 13,ProductDetailId = 121, Quantity = 20, TotalPrice = 2350}



                    });


                    await _context.Users.AddAsync(new User
                    {
                        userName = "admin",
                        password = "admmin",
                        firstName = "Admin",
                        lastName = "NaJa",
                        guid = Guid.NewGuid(),
                        email = "wwww@gmail.com",
                        mobile = "0899548xxx"
                    });

                    await _context.SaveChangesAsync();
                }
                catch { }
            }

        }

    }
}

